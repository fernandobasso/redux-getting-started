/* eslint-disable no-unused-vars */
import expect from 'expect';
import { log, info, warn, freeze } from './lib';

// 1. Test default state with no action and non-existing action.
// 2. Test state with no action and non-existing action.
// 3. Test default state with each possible action type.
// 4. Test state with each possible action type.
//
// Examples of possible action types:
//    - increment, decrement
//    - add, remove, toggle


info('✔ All tests passed!');
