
//
// Store Methods: getState(), dispatch(), and subscribe()
// ======================================================
//
// https://egghead.io/lessons/react-redux-store-methods-getstate-dispatch-and-subscribe
//

/* eslint-disable no-unused-vars */
import expect from 'expect';
import { createStore } from 'redux';
import { log, info, warn, freeze } from './lib';

// counter :: number -> object -> number
const counter = (state = 0, action) => {
  const { type } = action;

  switch (type) {
    case 'INCR':
      return state + 1;
    case 'DECR':
      return state - 1;
    default:
      return state;
  }
};

const store = createStore(counter);

const render = (label) => () => {
  document.body.innerText = `${label}: ${store.getState()}`;
};

const debug = () => {
  log('state:', store.getState());
}

store.subscribe(render('count value'));
store.subscribe(debug);

render('count value')();
debug();

document.addEventListener('click', () => {
  store.dispatch({ type: 'INCR' });
}, false);

