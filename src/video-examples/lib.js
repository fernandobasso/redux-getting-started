export const log = console.log.bind(console);
export const info = console.info.bind(console);
export const warn = console.warn.bind(console);
export const err = console.error.bind(console);

export const freeze = obj => {

  // Retrieve the property names defined on obj
  var propNames = Object.getOwnPropertyNames(obj);

  // Freeze properties before freezing self
  for (let name of propNames) {
    let value = obj[name];

    obj[name] = value && typeof value === 'object'
      ? freeze(value)
      : value;
  }

  return Object.freeze(obj);
};


