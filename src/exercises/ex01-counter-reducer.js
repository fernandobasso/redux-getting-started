
//
// EXERCISE:
// Create a `counter` reducer that passes the tests implemented below.
//

/* eslint-disable no-unused-vars */
import expect from 'expect';
import { log, info, warn, freeze } from './lib';

// counter :: number -> object -> number
const counter = (state, action) => {
  return undefined;
};


// 1. Test default state with no action and non-existing action.
expect(
  counter(undefined, {})
).toEqual(0);

expect(
  counter(undefined, { type: 'INCR' })
).toEqual(1);


// // 2. Test state with no action and non-existing action.
expect(
  counter(3, {})
).toEqual(3);

expect(
  counter(5, { type: 'NON_EXISTING' })
).toEqual(5);


// 3. Test default state with each possible action type.
expect(
  counter(undefined, { type: 'INCR' })
).toEqual(1);

expect(
  counter(undefined, { type: 'DECR' })
).toEqual(-1);


// 4. Test state with each possible action type.
expect(
  counter(5, { type: 'INCR' })
).toEqual(6);

expect(
  counter(5, { type: 'DECR' })
).toEqual(4);



info('✔ All tests passed!');


