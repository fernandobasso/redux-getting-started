# Exercises

Exercises created from the content of the videos.

- [Exercises](#exercises)
  - [General Notes](#general-notes)
    - [Node Version](#node-version)
    - [How To Practice](#how-to-practice)


## General Notes

### Node Version

Remember to use the proper version of node (we are using experimental ES6 modules support in Node!):

```bash
# Will read .nvmrc file.
nvm use
```

### How To Practice

You can create a practice file by running something like:

```bash
npm run ex01:make
```

Then you open `./practice/ex01-something.js`, start coding, and run the tests on it:

```bash
npm run ex01:check
```

If all tests for that exercise passed, congratulations!

If you want to clear the exercise one to start over, or to practice again from scratch, just do:

```bash
npm run ex01:clean
```

