
/* eslint-disable no-unused-vars */
import expect from 'expect';
import { log, info, warn, freeze } from './lib';

// counter :: number -> object -> number
const counter = (state = 0, action) => {
  const { type } = action;

  switch (type) {
    case 'INCR':
      return state + 1;
    case 'DECR':
      return state - 1;
    default:
      return state;
  }
};

//
// Given the `counter` reducer above, use Redux to create store and
// update the body of the page with the state. Update the state on
// each click on the body of the page.
//

