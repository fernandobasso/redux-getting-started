/*

EXERCISE:
---------
Create a `counter` reducer that passes the implemented tests.

TIPS:
-----
1. You can give a default value to the state parameter.

2. You may use either if/else if/else or a switch statement
   inside the reducer to respond to different action types.

3. Do not forget to handle the cases for when the action type
   is missing or invalid.

*/

// counter :: number -> object -> number
export const counter = (state, action) => {
  return undefined;
};


