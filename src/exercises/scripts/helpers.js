export const l = console.log.bind(console); // eslint-disable-line no-console
export const e = console.error.bind(console); // eslint-disable-line no-console

/**
 * Gets the filename from the argv arguments.
 *
 * @param {array} argv
 * @return {string} filename
 */
export const getFilename = (argv) => {
  if (argv.length < 3) return undefined;

  // Template argument.
  const filename = argv[2];

  return filename;
};
