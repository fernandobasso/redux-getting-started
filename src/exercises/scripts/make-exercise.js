
import {
  copyFileSync,
  constants as fsConstants, // <1>
} from 'fs';

import {
  l,
  e,
  getFilename,
} from './helpers';

const { COPYFILE_EXCL } = fsConstants; // <1>

/**
 * Copy the exercise template to the practice directory.
 *
 * @param {string} filename
 * @return {mixed} boolean|undefined
 */
const makeExercise = (filename) => {
  if (filename === undefined) {
    l('== makeExercise(): No filename provided. Bailing out.');
  }

  // <2>
  const source = `./templates/${filename}`;
  const destination = `./practice/${filename}`;

  l(
    source,
    destination,
  )

  try {
    copyFileSync(source, destination, COPYFILE_EXCL);
  }
  catch (err) {
    if (err.code === 'EEXIST') {
      l(`== makeExercise(): ${destination} already exists. Not overriding.`);
      return false;
    }

    throw err;
  }
};


makeExercise(getFilename(process.argv));


/*

<1>: Because we are using ES6 modules with --experimental-modules in Node.js v12,
     we would need a hackish way to get `__filename` and `__dirname`. Not worth
     the trouble for now. See:

     https://stackoverflow.com/questions/46745014/alternative-for-dirname-in-node-when-using-the-experimental-modules-flag

     https://nodejs.org/api/esm.html#esm_no_code_require_code_code_exports_code_code_module_exports_code_code_filename_code_code_dirname_code


<2>: We cannot destructure ES6 imports:

     https://github.com/babel/babel/issues/4996#issuecomment-266978120

*/
