
import {
  unlinkSync,
} from 'fs';

import {
  l,
  e,
  getFilename,
} from './helpers';

/**
 * Destroys the exercise file.
 *
 * @param {string} filename
 * @return {mixed} boolean|undefined
 */
const destroyExercise = (filename) => {
  if (filename === undefined) {
    e('== destroyExercise(): No filename provided. Bailing out.');
  }

  const target = `./practice/${filename}`;

  try {
    unlinkSync(target);
  }
  catch (err) {
    if (err.code === 'ENOENT') {
      e(`== destroyExercise(): ${target} doesn't exist. Nothing to remove.`);
      return false;
    }

    throw err;
  }
};


destroyExercise(getFilename(process.argv));
