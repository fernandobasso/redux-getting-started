
import {
  counter
} from '../practice/ex01-counter-reducer';

describe('ex01-counter-reducer', () => {
  it('should keep the default state with default state and no action type', () => {
    expect(counter(undefined, {})).toEqual(0);
  });

  it('should keep the default state with default state and non-existing action type', () => {
    expect(counter(undefined, { type: 'TOMB_RAIDER_1996' })).toEqual(0);
  });

  it('should keep the current state with no action type', () => {
    expect(counter(3, {})).toEqual(3);
  });

  it('should return the current state when non-existing action', () => {
    expect(counter(3, { type: 'LARA_CROFT' })).toEqual(3);
  });

  it('should increment the default state', () => {
    expect(counter(undefined, { type: 'INCR' })).toEqual(1);
  });

  it('should decrement the default state', () => {
    expect(counter(undefined, { type: 'DECR' })).toEqual(-1);
  });

  it('should increment the current state', () => {
    expect(counter(3, { type: 'INCR' })).toEqual(4);
  });

  it('should decrement the default state', () => {
    expect(counter(3, { type: 'DECR' })).toEqual(2);
  });
});
