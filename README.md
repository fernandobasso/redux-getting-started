# Getting Started With Redux

By Redux creator Dan Abramov.

https://egghead.io/courses/getting-started-with-redux

- [Getting Started With Redux](#getting-started-with-redux)
  - [Practicing](#practicing)
  - [Video 1: the single immutable state tree](#video-1-the-single-immutable-state-tree)
  - [Video 2: describe state changes with actions](#video-2-describe-state-changes-with-actions)
  - [Video 3: pure and impure functions](#video-3-pure-and-impure-functions)
  - [Video 4: the reducer function](#video-4-the-reducer-function)
  - [Lesson 5: Writing a Counter Reducer with Tests](#lesson-5-writing-a-counter-reducer-with-tests)
  - [Video 6: Store Methods: getState(), dispatch(), and subscribe()](#video-6-store-methods-getstate-dispatch-and-subscribe)
  - [Video 7: Implementing Store from Scratch](#video-7-implementing-store-from-scratch)


## Practicing

Take a look at the [exercises readme file](src/exercises/README.md) inside `src/exercises/` directory.

## Video 1: the single immutable state tree

Everything that changes in the application, including the data and the UI state is contained in a single object: the state tree. And that tree is immutable.


## Video 2: describe state changes with actions

The state tree is read-only. You cannot modify or write to it. You dispatch an action telling redux to create a new state tree with the modifications. An action is a plain JavaScript object describing the change.

The state is the minimal representation of the data in an app. The action is the minimal representation of the changes to that data.

The structure of the action object is up to you. Only the `type` property is required (and it must not be undefined).

To describe state mutations, you have to write a function that takes the previous state of the app, the action being dispatched, and returns the next state of the app. This function has to be pure. This function is called the “Reducer.”

## Video 3: pure and impure functions

Redux actions must be pure functions.


## Video 4: the reducer function

The reducer _must_ be a pure function.

Redux does not change the state. It actually calculates a new state based on the previous state.

To update the state of the app, you write a _pure function_ that takes the previous state of the app, the action being dispatched, and calculates and returns the next state. This is the reducer function.


## Lesson 5: Writing a Counter Reducer with Tests

https://egghead.io/lessons/react-redux-writing-a-counter-reducer-with-tests

dan-abramov-arrow-function-semantic.png

![Q-A on arrow function being more semantic](src/imgs/dan-abramov-arrow-function-semantic.png)

https://medium.com/@ryanflorence/functions-without-function-bc356ed34a2f

https://twitter.com/sebmarkbage/status/638531311477522433


## Video 6: Store Methods: getState(), dispatch(), and subscribe()

https://egghead.io/lessons/react-redux-store-methods-getstate-dispatch-and-subscribe

You create a store with `createStore` passing a reducer as parameter. Then, from the store, you can `subscribe` to a function that uses `getState` to update the UI. The store provides the methods `subscribe`, `getState` and `dispatch`.

## Video 7: Implementing Store from Scratch

https://egghead.io/lessons/react-redux-implementing-store-from-scratch

